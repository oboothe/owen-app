export default {
  url: 'https://dev-78052755.okta.com',
  issuer: 'https://dev-78052755.okta.com/oauth2/default',
  redirect_uri: window.location.origin + '/implicit/callback',
  client_id: '0oa18vpffwn1ODlZV5d7'
};