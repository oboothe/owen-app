const okta = require('@okta/okta-sdk-nodejs');
const express = require('express');

const router = express.Router();

const client = new okta.Client({
  orgUrl: process.env.OKTA_ORG_URL,
  token: process.env.OKTA_TOKEN
});

// Take the user to the homepage if they're not logged in
router.use('/', (req, res, next) => {
  if (!req.userContext) {
    return res.redirect('/');
  }

  next();
});

router.get('/', (req, res) => {
  const { userContext } = req;
  res.render('testpage', { userContext });
});

router.post('/', async (req, res) => {
  const { body } = req;

    res.redirect('/');
});

module.exports = router;